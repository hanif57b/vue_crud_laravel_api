<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Blog::get();
        // return response()->json([
        //     'success' => 200,
        //     'data' => $blog
        // ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'bname' => 'required',
            'bdescription' => 'required',
            'bpublish' => 'required'
        ]);
        $blog = Blog::create([
          'bname' => request('bname'),
          'bdescription' => request('bdescription'),
          'bpublish' => request('bpublish'),
        ]);

        return response()->json([
            'Success' => 200,
            'data' => $blog
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return Blog::where('id', $id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        if($blog){
            return response()->json($blog, 200);
        } else {
            return response()->json('Fail', 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        // $this->validate($request,[
        //     'bname' => 'required',
        //     'bdescription' => 'required',
        //     'bpublish' => 'required'
        // ]);

        $blog->update($request->all());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
         $blog->delete();
         return response()->json([
             'Sucessfully Deleted' => 200,
             'data' => $blog
         ]);

    }
}
